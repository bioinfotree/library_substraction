### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

extern ../phase_3/CDNA1_uniq.fasta as CDNA1
extern ../phase_3/CDNA2_uniq.fasta as CDNA2
extern ../phase_1/assembly.fasta as ASSEMBLY

CDNA1.fasta: $(CDNA1)
	ln -sf $< $@

CDNA2.fasta: $(CDNA2)
	ln -sf $< $@

assembly.fasta: $(ASSEMBLY)
	ln -sf $< $@



# the algorithm is correct, the order of the contigs is maintained during the counts
capture_histories.tab: assembly.fasta CDNA1.fasta CDNA2.fasta
	paste \
	<($(call count_capture,$^1,$^3)) \
	<($(call count_capture,$^1,$^2)) \
	| sed 1'i\CDNA3\tCDNA4'> $@



# the number of contigs common to both libraries is given by:
# TOTAL-CDNA1specific-CDNA2specific. 
# But it should be: 
# TOTAL-CDNA1specific - CDNA2specific-1e-50_mutual_cov.lst{phase_1} - min(CDNA1_mutual_uniq.lst{phase_2},CDNA2_mutual_uniq.lst{phase_2}) - min(nr_mutual.lst{phase_3})
# since only half of the contigs subtracted at each stage, should end in the common fraction. 
# But, since the estimation of population size varies so insignificant compared to 
# the standard error, in 2 cases, it was decided to calculate the specific fraction, 
# in the simplest way.
define count_capture
	cat $1 $2 \
	| fasta2tab \
	| cut -f 1 \
	| bsort \
	| uniq -c \
	| tr -s " " "\t" \
	| bawk '!/^[$$,\#+]/ \
	{ \
	if ( $$2 == 1 ) \
	{print 1;} \
	else if ( $$2 == 2 ) \
	{print 0;} \
	else {exit;} \
	}'
endef


estimated_abundance.tab: capture_histories.tab
	abundance < $< > $@


.PHONY: test
test: capture_histories.tab
	stat_base --total 3 <$^1








# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += CDNA1.fasta \
	 CDNA2.fasta \
	 assembly.fasta \
	 capture_histories.tab \
	 estimated_abundance.tab

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files and directories in the current directory 
# that are normally created by building the program.
CLEAN += CDNA1.fasta \
	 CDNA2.fasta \
	 assembly.fasta \
	 capture_histories.tab \
	 estimated_abundance.tab






######################################################################
### phase_3.mk ends here
