### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

extern ../phase_3/CDNA1_uniq.fasta as CDNA1
extern ../phase_3/CDNA2_uniq.fasta as CDNA2

extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.gene.lst as GENE_DANIO_HIT_LST
extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.subject.lst as CDNA_DANIO_HIT_LST


extern ../../../../annotation/dataset/$(PRJ)/phase_2/danio_EST.tab as DANIO_HIT_TAB



CDNA1.fasta: $(CDNA1)
	ln -sf $< $@

CDNA2.fasta: $(CDNA2)
	ln -sf $< $@

gene.lst: $(GENE_DANIO_HIT_LST)
	ln -sf $< $@

CDNA.lst: $(CDNA_DANIO_HIT_LST)
	ln -sf $< $@

blast_hit.tab: $(DANIO_HIT_TAB)
	ln -sf $< $@




background.lst: gene.lst
	cut -d ":" -f 2 $< > $@

background_CDNA.lst: CDNA.lst
	tr -s " " \\t < $< | cut -f 3 > $@


gene%.lst: CDNA%.fasta blast_hit.tab
	grep --fixed-strings --word-regexp --file <(fasta2tab < $< | cut -f1 | bsort) <(bsort --key=6,6 $^2) \
	| cut -f 9 \
	| cut -d " " -f 4 \
	| bsort \
	| uniq -c \
	| cut -d ":" -f 2 > $@

CDNA%.lst: CDNA%.fasta blast_hit.tab
	grep --fixed-strings --word-regexp --file <(fasta2tab < $< | cut -f1 | bsort) <(bsort --key=6,6 $^2) \
	| cut -f 9 \
	| cut -d " " -f 1 \
	| bsort \
	| uniq -c \
	| tr -s " " \\t \
	| cut -f 3 > $@


.PHONY: test
test: capture_histories.tab
	stat_base --total 3 <$^1








# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += CDNA1.fasta \
	 CDNA2.fasta \
	 gene.lst \
	 blast_hit.tab \
	 CDNA1.lst \
	 CDNA2.lst \
	 background.lst \
	 background_CDNA.lst \
	 gene1.lst \
	 gene2.lst

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files and directories in the current directory 
# that are normally created by building the program.
CLEAN += CDNA1.fasta \
	 CDNA2.fasta \
	 gene.lst \
	 blast_hit.tab \
	 CDNA1.lst \
	 CDNA2.lst \
	 background.lst \
	 background_CDNA.lst \
	 CDNA.lst \
	 gene1.lst \
	 gene2.lst







######################################################################
### phase_3.mk ends here
