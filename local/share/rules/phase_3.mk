### phase_3.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/annotation

extern ../../../../annotation/dataset/$(PRJ)/phase_1/nr.xml.gz as NR

extern ../phase_2/CDNA1_uniq.fasta as CDNA1
extern ../phase_2/CDNA2_uniq.fasta as CDNA2

INDIRECT_FILTER_EVAL ?=

INDIRECT_QUERY_COV ?=

# links
nr.xml.gz: $(NR)
	ln -sf $< .
CDNA1.fasta: $(CDNA1)
	ln -sf $< $@

CDNA2.fasta: $(CDNA2)
	ln -sf $< $@


# WARNING: -a hit_id,length,hit_def
%.tab: %.xml.gz
	zcat -dc $< | blast_fields -f -r query,query_length -a hit_id,length,hit_def -p align_length,bits,expect,frame,gaps,identities,query_end,query_start,sbjct_end,sbjct_start,score -q $(INDIRECT_QUERY_COV) -e $(INDIRECT_FILTER_EVAL) > $@

CDNA%_nr.tab: CDNA%.fasta nr.tab
	translate -k -a $^2 1 < <(fasta2tab <$< | cut -f 1) > $@


# unexpand requires at least two spaces to convert to tabs so use tr.
%_common.lst: CDNA1_%.tab CDNA2_%.tab
	comm -1 -2 \
	<(cut -f 3 $< | bsort) \
	<(cut -f 3 $^2 | bsort) > $@


%_mutual.lst: %_common.lst CDNA1_%.tab CDNA2_%.tab
	paste \
	<(grep --file $< --fixed-strings --word-regexp < $^2 \
	| cut -f 1 \
	| bsort \
	| uniq) \
	<(grep --file $< --fixed-strings --word-regexp < $^3 \
	| cut -f 1 \
	| bsort \
	| uniq) \
	| sed '1i# CDNA1\tCDNA2' > $@




# Join the linked queries throught all DBs
# Filter them by uniqueness

# The cut field is generated by the number of the library - 2:
# CDNA1 -> cut field = 1
# CDNA2 -> cut field = 2
CDNA%_mutual_uniq.lst: nr_mutual.lst
	$(call get_mutual_uniq,$<,$*,$@)

# Arguments: 1: list of all prerequisites
#            2: column to cut as defined in %_mutual.lst target:
#               1 for CDNA1, 2 for CDNA2
#            3: target to build
#            Returns:   nothing, build target
define get_mutual_uniq
	bawk '!/^[$$,\#+]/ { \
	print $$0; \
	}' $1 \
	| cut -f $2 \
	| sed '/^$$/d' \
	| bsort \
	| uniq > $3
endef

# Subtract this queries from their own set
# Extract the sequences that remain
CDNA%_uniq.fasta: CDNA%_mutual_uniq.lst CDNA%.fasta
	comm -1 -3 $< \
	<(fasta2tab < $^2 | cut -f 1 | bsort) \
	| get_fasta --from-file <(cat -) < $^2 > $@



.PHONY: test
test:
	@echo $(COMM_LST)








# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += nr.xml.gz \
	 CDNA1.fasta \
	 CDNA2.fasta \
	 nr.tab \
	 CDNA1_nr.tab \
	 CDNA2_nr.tab \
	 nr_common.lst \
	 nr_mutual.lst \
	 CDNA1_mutual_uniq.lst \
	 CDNA2_mutual_uniq.lst \
	 CDNA1_uniq.fasta \
	 CDNA2_uniq.fasta

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files and directories in the current directory 
# that are normally created by building the program.
CLEAN += nr.xml.gz \
	 CDNA1.fasta \
	 CDNA2.fasta \
	 nr.tab \
	 CDNA1_nr.tab \
	 CDNA2_nr.tab \
	 nr_common.lst \
	 nr_mutual.lst \
	 CDNA1_mutual_uniq.lst \
	 CDNA2_mutual_uniq.lst \
	 CDNA1_uniq.fasta \
	 CDNA2_uniq.fasta






######################################################################
### phase_3.mk ends here
