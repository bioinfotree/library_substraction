### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/annotation

CONTIG_READ_LIST ?=

ASSEMBLY_FASTA ?=

CPUS ?=

EVALUES ?=

LABEL1 ?=

LABEL2 ?=

MUTUAL_FILTER_EVAL ?=

MUTUAL_QUERY_COV ?=

# create links for all the necessary files
contig_read_lst.lst: $(CONTIG_READ_LIST)
	ln -sf $< $@

assembly.fasta: $(ASSEMBLY_FASTA)
	 ln -sf $< $@

# divide contigs by library belonging:
# 1) library
# 2) library
# 3) both libraries
cdna1_specific.lst: contig_read_lst.lst
	bawk -v label1=$(LABEL1) -v label2=$(LABEL2) -v label1file=$@ -v label2file=cdna2_specific.lst -v bothlabels=aspecific.lst 'BEGIN {pattern1="^"label1"_"; pattern2="^"label2"_";} !/^[$$,\#+]/ \
	{ \
	# Awk rapid Hash Function \
	hash[$$1] = hash[$$1]"\t"$$2; \
	} \
	END { \
	for (x in hash) \
	{ \
	cdna3=0; cdna4=0; \
	split(hash[x],a,"\t"); \
	for (i in a) \
	{ \
	if ( a[i] ~ label1 ) { cdna3++; } \
	else if ( a[i] ~ label2 ) { cdna4++; } \
	} \
	#print length(a),cdna3,cdna4; \
	if ( cdna4==0 && cdna3==(length(a)-1) ) { print x hash[x] > label1file; } \
	else if ( cdna3==0 && cdna4==(length(a)-1) ) { print x hash[x] > label2file; } \
	else { print x hash[x] > bothlabels; } \
	} \
	}' <$<

cdna2_specific.lst: cdna1_specific.lst
	touch $@

aspecific.lst: cdna1_specific.lst
	touch $@


cdna%.fasta: cdna%_specific.lst assembly.fasta
	get_fasta --from-file <(cut -f 1 $<) < $^2 > $@

# make DB for BLASTN
cdna%.flag: cdna%.fasta
	mkdir -p $(basename $<); \
	cd $(basename $<); \
	ln -sf ../$< $(basename $<); \
	makeblastdb -in $(basename $<) -dbtype nucl -title $(basename $<); \
	cd ..; \
	touch $@





define search_DB
	blastn -evalue $1 -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $2 -db ./$(basename $3)/$(basename $3) -out $(basename $4); \
	if gzip -cv $(basename $4) > $4; \
	then \
	rm $(basename $4); \
	fi
endef

CDNA1_BLANSTN_XML_GZ = $(addsuffix _cdna1.xml.gz,$(EVALUES))
%_cdna1.xml.gz: cdna1.fasta cdna2.flag
	!threads
	$(call search_DB,$*,$<,$^2,$@)


CDNA2_BLANSTN_XML_GZ = $(addsuffix _cdna2.xml.gz,$(EVALUES))
%_cdna2.xml.gz: cdna2.fasta cdna1.flag
	!threads
	$(call search_DB,$*,$<,$^2,$@)






define parse_result
	zcat -dc $1 | blast_fields -f -r query,query_length -a hit_def,length -p align_length,bits,expect,frame,gaps,identities,query_end,query_start,sbjct_end,sbjct_start,score -q $(MUTUAL_QUERY_COV) > $2
endef

CDNA1_BLANSTN_TAB = $(addsuffix _cdna1.tab,$(EVALUES))
%_cdna1.tab: %_cdna1.xml.gz
	$(call parse_result, $<, $@)


CDNA2_BLANSTN_TAB = $(addsuffix _cdna2.tab,$(EVALUES))
%_cdna2.tab: %_cdna2.xml.gz
	$(call parse_result, $<, $@)






# select the pairs of sequences specific to each library, which show a mutual 
# alignment above the threshold of query coverage defined blast_fields
# 1) select sbj qry from the first
# 2) select qry sbj from the second (excange columns)
# 3) check for common lines
MUTUAL_COV = $(addsuffix _mutual_cov.lst,$(EVALUES))
%_mutual_cov.lst: %_cdna1.tab %_cdna2.tab
	comm -1 -2  <(cut -f 1,3 --output-delimiter ":" <$< | bsort) \
	<(cut -f 1,3 <$^2 | select_columns 2 1 | cut -f 1,2 --output-delimiter ":"| bsort) \
	| tr ":" "\t" > $@

# select sequence specific to each library that don't appears in respective 
# _mutual_cov.lst.
# In this case remove only the contigs which show a mutual alignment of more than 80%, with a significance <= $(MUTUAL_FILTER_EVAL).
cdna%_uniq.fasta: $(MUTUAL_FILTER_EVAL)_mutual_cov.lst cdna%_specific.lst cdna%.fasta
	comm -1 -3 <(cut -f $* <$< | bsort) <(cut -f 1 <$^2 | bsort) \
	| get_fasta --from-file <(cat -) < $^3 > $@




.PHONY: test
test: 1e-50_cdna3.tab cdna3_specific.lst
	@echo








# Standard Phony Targets for Users.
 
# This should be the default target.

# If I have a list of target, I report the list here. 
# For each element of the list, 
# make executes the rule that allows to build it.

# IMPORTANT: You must report all the target files for every
# rule
ALL   += cdna1.fasta \
	 cdna2.fasta \
	 cdna1.flag \
	 cdna2.flag \
	 $(CDNA1_BLANSTN_XML_GZ) \
	 $(CDNA2_BLANSTN_XML_GZ) \
	 $(CDNA1_BLANSTN_TAB) \
	 $(CDNA2_BLANSTN_TAB) \
	 $(MUTUAL_COV) \
	 cdna1_uniq.fasta \
	 cdna2_uniq.fasta

# The dependency of the targhet listened here
# are treated as intermediate files. So they 
# they are automatically erased later on 
# after they are no longer needed. 
INTERMEDIATE += 




# Delete all files in the current directory 
# that are normally created by building the program.
CLEAN += contig_read_lst.lst \
	 cdna1_specific.lst \
	 cdna2_specific.lst \
	 aspecific.lst \
	 assembly.fasta \
	 cdna1.fasta \
	 cdna2.fasta \
	 cdna1.flag \
	 cdna2.flag \
	 $(CDNA1_BLANSTN_XML_GZ) \
	 $(CDNA2_BLANSTN_XML_GZ) \
	 $(CDNA1_BLANSTN_TAB) \
	 $(CDNA2_BLANSTN_TAB) \
	 $(MUTUAL_COV) \
	 cdna1_uniq.fasta \
	 cdna2_uniq.fasta \
	 cdna1 \
	 cdna2



######################################################################
### phase_1.mk ends here
