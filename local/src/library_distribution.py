# library_distribution.py --- 
# 
# Filename: library_distribution.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Thu Apr  7 12:52:22 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:


#!/usr/bin/env python

import re, sys, argparse, os
from PipeUtils import PipeUtil
# biopython sequence Input/Output module
from Bio import SeqIO

def main():

    args = arg_parser()

    num_tags = len(args.tags)
    
    d = {}
    
    for (index, line) in enumerate (open(args.contigreadlist[0])):
        line = line.split()
        # exclude comments 
        if line[0] != r"#":                    
            contig = line[0]
            read = line [1]

            counts = []
            for i in range(num_tags):
                counts.append(int(0))
                
            d.setdefault(contig, counts)    

            for i in range(num_tags):
                # search tag at the beginning of the reads name
                compiledReForFirstTag = re.compile(r"^" + args.tags[i])
                if compiledReForFirstTag.search(read):
                    d[contig][i] += 1

    # execute only if it is required to classify contigs.
    if args.divide != None:

        # contains names
        new_fasta_file_lst = []
        new_qual_file_lst = []

        # contains Bio SeqRecord obects
        new_fasta_lst = []
        new_qual_lst = []

        both_assembly_fasta = r""
        both_assembly_qual = r""

        for path in args.divide:
            # take file name from path
            file_name = os.path.split(path)[-1]
            # control extension for fasta
            if file_name.split(r".")[-1] == r"fasta":
                fasta_file_name = file_name

                for i in range(num_tags):
                    new_fasta_file_lst.append(args.tags[i] + "_" + fasta_file_name)
                    new_fasta_lst.append([])
                both_assembly_fasta = r"both_" + fasta_file_name

            # control extension for qual
            if file_name.split(r".")[-1] == r"qual":
                qual_file_name = file_name
                for i in range(num_tags):
                    new_qual_file_lst.append(args.tags[i] + "_" + qual_file_name)
                    new_qual_lst.append([])
                both_assembly_qual = r"both_" + qual_file_name


        # get dictionary from files that are just 2
        util = PipeUtil()
        fasta_dict = util.get_fasta_index(args.divide[0])
        qual_dict = util.get_qual_index(args.divide[1])

        both_assembly_qual_lst = []
        both_assembly_fasta_lst = []


    print "Contig\t%s" % (u"\t".join(args.tags))
    
    for contig in d.keys():
        counts = d.get(contig)
        # if tag1Counts != 0 or tag2Counts != 0:        
        total = sum(counts)
        # are shown only contigs formed by a number of reads > threshold
        if (total >= args.threshold):
            tag1P = (1.0 * counts[0]) / total

            str_counts = []
            for n in counts:
                str_counts.append(str(n))
                
            print "%s\t%s\t%f" % (contig, u"\t".join(str_counts), tag1P)

        # execute only if it is required to classify contigs.
        if args.divide != None:
            # test if all but one element on al list are 0
            # if true return it's index alse return 0
            test, i = all_but_one_zero(counts)
            # are shown and/or reported only contigs formed by reads from single library
            # and composed of a number of reads > threshold
            if (test == True) and (total >= args.threshold):
                new_fasta_lst[i].append(fasta_dict.get(contig))
                new_qual_lst[i].append(qual_dict.get(contig))
            else:
                both_assembly_fasta_lst.append(fasta_dict.get(contig))
                both_assembly_qual_lst.append(qual_dict.get(contig))
                
    # stop execution if stop the execution
    # if it is not required to classify contigs.
    if args.divide == None:
        sys.exit()
    
    new_fasta_count = []
    new_qual_count = []
    for i in range(num_tags):
        new_fasta_count = SeqIO.write(new_fasta_lst[i], open(new_fasta_file_lst[i], r"w"), \
                                    r"fasta")  
        new_qual_count = SeqIO.write(new_qual_lst[i], open(new_qual_file_lst[i], r"w"), \
                              r"qual")
        print u"%s: fasta = %i\tqual = %i" %(args.tags[i], new_fasta_count, new_qual_count)

    both_fasta_count = SeqIO.write(both_assembly_fasta_lst, open(both_assembly_fasta, r"w"), \
                             r"fasta")
    both_qual_count = SeqIO.write(both_assembly_qual_lst, open(both_assembly_qual, r"w"), \
                              r"qual")
    print u"both: fasta = %i\tqual = %i" %(both_fasta_count, both_qual_count)







def arg_parser():
    """
    Function for parsin arguments
    """
    parser = argparse.ArgumentParser(description="Rank contigs based on tags that mark its reads")
    # +: all command-line args are gathered into a list.
    #    an error message will be generated if there wasn't at least one command-line arg present 
    parser.add_argument("-t", "--tags", action="store", dest="tags", nargs="+", help="Reads tags", required=True)
    parser.add_argument("-l", "--contigreadlist", action="store", dest="contigreadlist", nargs=1, help="Tab delimited file with list of reads forming each contig", required=True)
    parser.add_argument("-s", "--threshold", action="store", dest="threshold", help="Are shown and/or reported only contigs formed from a number of reads > threshold", required=False, type=int, default=1)
    parser.add_argument("-d", "--divide", action="store", dest="divide", nargs=2, help="Divides the assembly in fasta and quality format in different fasta and quality files, according to tags. First fasta file, second quality file", required=False)
    
    args = parser.parse_args()
    # at least one arguments should be introduced
    #if (args.compile==True) and (args.reset==True):
        #parser.error("\n\n\tMissing parameters!\n\n")
        
    return args



# test if all but one element on al list are 0
# if true return it's index alse return 0
def all_but_one_zero(lst):
    indexes_lst = []
    for (i, elem) in enumerate(lst):
        if elem != 0:
            indexes_lst.append(i)

    if len(indexes_lst) == 1:   
        return True, indexes_lst[0]
    else:
        return False, 0




if __name__ == "__main__":
    main()

# 
# library_distribution.py ends here
