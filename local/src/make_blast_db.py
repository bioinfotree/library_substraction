# make_blast.db.py --- 
# 
# Filename: make_blast.db.py
# Description: 
# Author: Michele
# Maintainer: 
# Created: Fri Apr  8 16:55:10 2011 (+0200)
# Version: 
# Last-Updated: 
#           By: 
#     Update #: 0
# URL: 
# Keywords: 
# Compatibility: 
# 
# 

# Commentary: 
# 
# 
# 
# 

# Change Log:
# 
# 
# 
# 
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street, Fifth
# Floor, Boston, MA 02110-1301, USA.
# 
# 

# Code:
from blast_tools.NCBI_BLAST import NCBIBLAST
import sys

# class test
def main():
    # in fasta file
    in_fasta = sys.argv[1]

    # set out blast database 
    out_file = sys.argv[2]
    
    # set options for creating database
    common_options = [r""]
    prog_specific_options = [r"-in %s" %(in_fasta), r"-hash_index", u"-title ""%s""" %(out_file)]

    # create object
    ncbiblast = NCBIBLAST()

    # try options
    ncbiblast.set_blast_bin(r"makeblastdb")
    new_db = ncbiblast.set_out_file(out_file)
    ncbiblast.set_dbtype(str(sys.argv[3]))
    print ncbiblast.set_blast_settings(common_options, prog_specific_options)
    ncbiblast.search()

    return 0



if __name__ == "__main__":
    main()

# 
# make_blast.db.py ends here
